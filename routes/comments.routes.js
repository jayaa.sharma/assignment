const express = require("express");
const router = express.Router();
const CommentController = require("../controllers/comments.controller");
const { create } = require("../models/users.model");

// Comment routes
router.post("/", CommentController.add);
router.put("/:commentId", CommentController.update);
router.delete("/:commentId", CommentController.delete);

//Get all comments by a user
router.get("/:userId", CommentController.getList);

// Reply routes
router.get("/:commentId/reply", CommentController.getReplyList);
router.post("/:commentId/reply", CommentController.addReply);
router.put("/:commentId/reply/:replyId", CommentController.updateReply);
router.delete("/:commentId/reply/:replyId", CommentController.deleteReply);

module.exports = router;
